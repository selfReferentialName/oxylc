#!/bin/bash
cd `dirname ${BASH_SOURCE[0]}`

# run after build.sh
# only this needs sudo

cp -Lf oxyl $1/bin/oxyl
chmod 755 $1/bin/oxyl
cp -Lf oxylc $1/bin/oxylc
chmod 755 $1/bin/oxylc
cp -Lf oxylld $1/bin/oxylld
chmod 755 $1/bin/oxylld
mkdir -p $1/share/oxyl
chmod 755 $1/share/oxyl
cp -Lf oxylrt/liboxylrt.o $1/share/oxyl/rt.o
cp -Lf oxylrt/oxylrt.ld $1/share/oxyl/link.ld
chmod 644 $1/share/oxyl/*
