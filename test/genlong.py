#/usr/bin/python
import argparse as ap

parser = ap.ArgumentParser(description="Generate a benchmarking file.")
parser.add_argument("-n", dest="num", action="store", default=1000000)
parser.add_argument("file", action="store", default="long.xyl")
args = parser.parse_args()

outfile = open(args.file, "w")

print("void long_function void", file=outfile)

for i in range(1, int(args.num)):
    print("\tint v" + str(i) + " = " + str(i), file=outfile)
