FROM ubuntu
MAINTAINER J Rain De Jager version: alpha1.1.3
RUN apt update && apt-get install -y opam clang cmake llvm binutils qemu m4 git pkg-config nasm && opam init -y && eval `opam config env`
RUN opam update && opam install cmdliner core ocamlfind omake printbox ounit llvm semver
