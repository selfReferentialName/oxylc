setlocal shiftwidth=6
setlocal tabstop=6

" Oxyl needs tabs. DO NOT SET expandtab OR STUFF WILL BREAK
setlocal noexpandtab

setlocal autoindent
