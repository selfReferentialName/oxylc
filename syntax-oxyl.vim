" Oxyl syntax highlighting

set tabstop=4

" Builtin kinds
syn keyword xBuiltin void bool i8 i16 i32 int float string array vec u8 u16 char u64 posib

" Builtin functions
syn keyword xBiFunc own

" Block openers
" syn keyword xBlocker class if while as else elif

" One liner keywords
syn keyword xOneline context import include as

" Builtin functions
syn keyword xBuiltinFunc panic exit
" syn match xReserved '_[_A-Za-z0-9]*'
" hi def link xReserved xBuiltinFunc

" annotators
syn keyword xAnnotate pub ffi tail rec

syn match xInt '-?\d\+'
syn match xFstd '\d\+\.\d*'
syn match xFexp '\d\+\.?\d*[eE][-+]?\d\+'
syn match xChar '\'.'
syn region xString start='"' skip='\\"' end='"'

syn match xWordUpper '[A-Z][^\[\],.:;{}\(\) \t\n]*'
syn match xWordLower '[a-z][^\[\],.:;{}\(\) \t\n]*'

syn region xComment start='#' end='\n'

hi def link xBuiltin BType
hi def link xBlocker Conditional
hi def link xOneline PreProc
hi def link xBuiltinFunc PreProc
hi def link xAnnotate PreProc
hi def link xInt Number
hi def link xFstd Float
hi def link xFexp Float
hi def link xWordUpper UType
hi def link UType Type
hi def link BType Type
hi def link xWordLower Idientifier
hi def link xChar String
hi def link xString String
hi def link xComment Comment
