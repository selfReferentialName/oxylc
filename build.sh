#!/bin/bash
cd `dirname ${BASH_SOURCE[0]}`
set -e

echo "let prefix = \"$1\";;" > src/cfg.ml
if [ -n $DO_COVERAGE ]
then
	sed -e 's/bisect_ppx/#bisect_ppx/g' src/OMakefile >src/OMakefile_tmp
	mv -f src/OMakefile_tmp src/OMakefile
else
	sed -e 's/#+bisect_ppx/bisect_ppx/g' src/OMakefile >src/OMakefile_tmp
	mv -f src/OMakefile_tmp src/OMakefile
fi
omake
cd oxylrt
./build.sh $1
cd ../src
omake
omake test
./test
