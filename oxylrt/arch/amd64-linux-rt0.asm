section .text
global _start
global _exit
global _ewrite
extern entry

_exit:
	mov rdi, rax
	mov rax, 60
	syscall

_ewrite:
	mov rdx, rsi
	mov rsi, rdi
	mov rdi, 2
	mov rax, 1
	syscall
	ret

_start:
	xor rbp, rbp
	call entry
	mov rax, 60
	xor rdi, rdi
	syscall
