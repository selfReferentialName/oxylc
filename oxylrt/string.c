// TODO: WTF-8 functions

#include "oxylrt.h"

typedef struct packed array {
	uint32_t size;
	char *data;
} *Array;

static uint64_t get_size(String self) {
	return self->size;
}

static Opaque colon(String self, uint64_t index) {
	if (index >= self->size) {
		oxylrt__ewrite("String indexing bound error: ");
		oxylrt__ewrite_char(index);
		oxylrt__ewrite(" > ");
		oxylrt__ewrite_char(self->size);
		oxylrt__panic("\n");
	}
	return self->data[index];
}

static bool equal(String self, String right) {
	if (self->size != right->size) {
		return false;
	}
	for (uint64_t i = 0; i < self->size; i++) {
		if (self->data[i] != right->data[i]) {
			return false;
		}
	}
	return true;
}

struct type oxylrt__string = {
	.size = sizeof(struct string),
	.name = "string",
	.name_length = 6,
	.equal = equal
};
