#!/bin/bash
set -e

clang -O3 -ffreestanding -c *.c

rm arch/rt0.o
nasm -felf64 -o arch/rt0.o arch/amd64-linux-rt0.asm

ld -r -T oxylrt.ld -o liboxylrt.o arch/rt0.o oxylrt.o array.o string.o
