#include "oxylrt.h"

typedef struct packed array {
	Type *cont;
	uint32_t size;
	Opaque *data;
} *Array;

static uint64_t get_size(Array self) {
	return self->size;
}

static Opaque colon(Array self, int64_t index) {
	if (index >= self->size) {
		oxylrt__ewrite("Array indexing bound error: ");
		oxylrt__ewrite_char(index);
		oxylrt__ewrite(" > ");
		oxylrt__ewrite_char(self->size);
		oxylrt__panic("\n");
	}
	if (index < 0) {
		index = self->size + index;
	}
	return self->data[index];
}

static bool equal(Array self, Array right) {
	if (self->size != right->size) {
		return false;
	}
	for (uint64_t i = 0; i < self->size; i++) {
		if (!(self->cont->equal(self, right))) {
			return false;
		}
	}
	return true;
}

Type *oxylrt__array___ka(Type *self, Type *cont) {
	self->size = sizeof(struct array);
	self->name = "array";
	self->name_length = 5;
	self->equal = equal;
	return self;
}
