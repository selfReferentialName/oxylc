#ifndef OXYLRT_H
#define OXYLRT_H

#include <stdint.h>
#include <stdbool.h>
#include <stdnoreturn.h>
#include "oxylrt.h"

#define packed __attribute__((__packed__))

typedef void *Opaque;
typedef void *FunPtr;

typedef struct packed string {
	uint32_t size;
	char *data;
} *String;

typedef struct packed type {
	uint64_t size;
	char *name;
	uint16_t name_length;
	bool (*equal) (void*, void*);
} Type;

void _ewrite(char*, uint64_t);

noreturn void oxylrt__panic(char *);
noreturn void oxylrt__ccpanic(uint64_t, char **);
noreturn void _exit(int);

#endif
