	.text
	.file	"oxylrt.c"
	.globl	strlen                  # -- Begin function strlen
	.p2align	4, 0x90
	.type	strlen,@function
strlen:                                 # @strlen
	.cfi_startproc
# %bb.0:
	movq	$-1, %rax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, 1(%rdi,%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_1
# %bb.2:
	movl	%eax, %eax
	retq
.Lfunc_end0:
	.size	strlen, .Lfunc_end0-strlen
	.cfi_endproc
                                        # -- End function
	.globl	oxylrt__panic           # -- Begin function oxylrt__panic
	.p2align	4, 0x90
	.type	oxylrt__panic,@function
oxylrt__panic:                          # @oxylrt__panic
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	movq	$-1, %rax
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, 1(%rdi,%rax)
	leaq	1(%rax), %rax
	jne	.LBB1_1
# %bb.2:
	movl	%eax, %esi
	callq	_ewrite
	movl	$1, %edi
	callq	_exit
.Lfunc_end1:
	.size	oxylrt__panic, .Lfunc_end1-oxylrt__panic
	.cfi_endproc
                                        # -- End function
	.globl	oxylrt__ccpanic         # -- Begin function oxylrt__ccpanic
	.p2align	4, 0x90
	.type	oxylrt__ccpanic,@function
oxylrt__ccpanic:                        # @oxylrt__ccpanic
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	testq	%rdi, %rdi
	je	.LBB2_5
# %bb.1:
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	(%r14,%rbx,8), %rdi
	movq	$-1, %rax
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 1(%rdi,%rax)
	leaq	1(%rax), %rax
	jne	.LBB2_3
# %bb.4:                                #   in Loop: Header=BB2_2 Depth=1
	movl	%eax, %esi
	callq	_ewrite
	addq	$1, %rbx
	cmpq	%r15, %rbx
	jne	.LBB2_2
.LBB2_5:
	movl	$1, %edi
	callq	_exit
.Lfunc_end2:
	.size	oxylrt__ccpanic, .Lfunc_end2-oxylrt__ccpanic
	.cfi_endproc
                                        # -- End function
	.globl	_panic                  # -- Begin function _panic
	.p2align	4, 0x90
	.type	_panic,@function
_panic:                                 # @_panic
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdi
	callq	_ewrite
	movl	$2, %edi
	callq	_exit
.Lfunc_end3:
	.size	_panic, .Lfunc_end3-_panic
	.cfi_endproc
                                        # -- End function

	.ident	"clang version 9.0.0-2 (tags/RELEASE_900/final)"
	.section	".note.GNU-stack","",@progbits
	.addrsig
