#include "oxylrt.h"

uint64_t strlen(char *s) {
	int i;
	for (i = 0; s[i] != 0; i++);
	return i;
}

void oxylrt__ewrite(char *s) {
	_ewrite(s, strlen(s));
}

void oxylrt__ewrite_char(uint32_t v) {
	char output[11];
	for (int i = 0; i < 11; i++) {
		if (v > 0) {
			output[i] = '0' + v % 10;
			v = v / 10;
		} else {
			output[i] = 0;
		}
	}
	_ewrite(output, strlen(output));
}

noreturn void oxylrt__panic(char *s) {
	_ewrite(s, strlen(s));
	_exit(1);
}

struct packed str {
	uint32_t size;
	char *data;
};

noreturn void _panic(struct str* s) {
	_ewrite(s->data, s->size);
	_exit(2);
}
